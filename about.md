---
layout: left_col
title: About us
section: About
permalink: /about-node
pages:
  - href: '/about-node'
    title: First link
  - href: '/about-node'
    title: Second link  
  - href: '/about-node'
    title: Third link
  - href: '/about-node'
    title: First sub link  
    smaller: true
  - href: '/about-node'
    title: Second sub-link    
    smaller: true
  - href: '/about-node'
    title: Fourth link   

---

## Services

**Repository for high quality Translational Medicine data**

Integration of well-curated clinical and molecular data from cohorts and large consortia. Implementation of standardised electronic data capture, data harmonisation and curation.

----

**High-performance data access and computing services**

Sustainable access management according to well-defined criteria, in order to meet the security and accountability requirements of ELIXIR, of the General European data protection regulation (GDPR), and of the research community.  Platforms and tools to allow efficient data access and analysis.

---

## Support and user training

Workshops and courses on data management, curation, analytics and visualisation. Continuous education of software developers, data managers and researchers.
